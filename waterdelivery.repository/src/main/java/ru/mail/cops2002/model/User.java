/**
 * 
 */
package ru.mail.cops2002.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * @author ykypok
 *
 */
@Entity
@Table(name = "User", catalog = "waterdelivery")
public class User implements Serializable {

	private static final long serialVersionUID = -4043912749589627590L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_id", unique = true)
	private Integer id;
	@Column(name = "user_email", unique = true)
	private String email;
	@Column(name = "user_password")
	private String passsword;
	@OneToOne
	private UserInformation information;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPasssword() {
		return passsword;
	}

	public void setPasssword(String passsword) {
		this.passsword = passsword;
	}

	public UserInformation getInformation() {
		return information;
	}

	public void setInformation(UserInformation information) {
		this.information = information;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((passsword == null) ? 0 : passsword.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (passsword == null) {
			if (other.passsword != null)
				return false;
		} else if (!passsword.equals(other.passsword))
			return false;
		return true;
	}

}
